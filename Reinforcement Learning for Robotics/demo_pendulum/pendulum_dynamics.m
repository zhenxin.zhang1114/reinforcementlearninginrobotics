function y = pendulum_dynamics(x,u,dt)
%% Code

l = 1;    % [m]        length of pendulum
m = 1;    % [kg]       mass of pendulum
g = 9.82; % [m/s^2]    acceleration of gravity
b = 0.2; % [s*Nm/rad] friction coefficient

dx = zeros(2,size(x,2));

dx(1,:) = x(2,:);%angle
dx(2,:) = ( u - b*x(2,:) - m*g*l*sin(x(1,:))/2 ) / (m*l^2/3);%angle velocity


y = x + dx*dt;
