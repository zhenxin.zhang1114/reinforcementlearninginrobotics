function demo_pendulum
% A demo of iLQG/DDP with car-parking dynamics
clc;
close all

fprintf(['\nA demonstration of the iLQG algorithm '...
'with pendulum dynamics.\n'...
'for details see\nTassa, Mansard & Todorov, ICRA 2014\n'...
'\"Control-Limited Differential Dynamic Programming\"\n'])

% Set full_DDP=true to compute 2nd order derivatives of the 
% dynamics. This will make iterations more expensive, but 
% final convergence will be much faster (quadratic)
full_DDP = false;

% set up the optimization problem
dt = 0.01;
DYNCST  = @(x,u,i) pen_dyn_cst(x,u,dt,full_DDP);
T       = 500;              % horizon
x0      = [0;0];   % initial state
u0      = .1*randn(1,T);    % initial controls
Op.lims = [-5 5];        % control input limits (N/m)
Op.plot = -1;               % plot the derivatives as well

% === run the optimization!
[x,u]= iLQG(DYNCST, x0, u0, Op);

% animate the resulting trajectory
figure
for r = 1:size(u,2)
     draw_pendulum(x(1,r), u(r), 0,  ...
      ['Optimal control policy, T=' num2str(T*dt) ' sec'], ...
      '')
  pause(dt/10);
end

function y = pendulum_dynamics(x,u,dt)
%% Code

l = 1;    % [m]        length of pendulum
m = 1;    % [kg]       mass of pendulum
g = 9.82; % [m/s^2]    acceleration of gravity
b = 0.2; % [s*Nm/rad] friction coefficient

dx = zeros(2,size(x,2));

dx(1,:) = x(2,:);
dx(2,:) = ( u - b*x(2,:) - m*g*l*sin(x(1,:))/2 ) / (m*l^2/3);

y = x + dx*dt;

function c = pendulum_cost(x, u)
% cost function for inverted pendulum problem
% sum of 3 terms:
% lu: quadratic cost on controls
% lf: final cost on distance from target parking configuration
% lx: running cost on distance from origin to encourage tight turns

final = isnan(u(1,:));
u(:,final)  = 0;

cu  = 1e-3;         % control cost coefficients

cf  = [ .1  .1];    % final cost coefficients
pf  = [.01 .01]';    % smoothness scales for final cost

cx  = 1e-1*[1  1];          % running cost coefficients
px  = [.1 .1]';             % smoothness scales for running cost

% control cost
lu    = cu*u.^2;

% final cost
if any(final)
   llf      = cf*sabs(x(:,final)-[pi;0],pf);
   lf       = double(final);
   lf(final)= llf;
else
   lf    = 0;
end

% running cost
lx = cx*sabs(x(1:2,:)-[pi;0],px);

% total cost
c     = lu + lx + lf;

function y = sabs(x,p)
% smooth absolute-value function (a.k.a pseudo-Huber)
y = pp( sqrt(pp(x.^2,p.^2)), -p);


function [f,c,fx,fu,fxx,fxu,fuu,cx,cu,cxx,cxu,cuu] = pen_dyn_cst(x,u,dt,full_DDP)
% combine car dynamics and cost
% use helper function finite_difference() to compute derivatives

if nargout == 2
    f = pendulum_dynamics(x,u,dt);
    c = pendulum_cost(x,u);
else
    % state and control indices
    ix = 1:2;
    iu = 3;
    
    % dynamics first derivatives
    xu_dyn  = @(xu) pendulum_dynamics(xu(ix,:),xu(iu,:),dt);
    J       = finite_difference(xu_dyn, [x; u]);
    fx      = J(:,ix,:);
    fu      = J(:,iu,:);
    
    % dynamics second derivatives
    if full_DDP
        xu_Jcst = @(xu) finite_difference(xu_dyn, xu);
        JJ      = finite_difference(xu_Jcst, [x; u]);
        JJ      = reshape(JJ, [4 6 size(J)]);
        JJ      = 0.5*(JJ + permute(JJ,[1 3 2 4])); %symmetrize
        fxx     = JJ(:,ix,ix,:);
        fxu     = JJ(:,ix,iu,:);
        fuu     = JJ(:,iu,iu,:);    
    else
        [fxx,fxu,fuu] = deal([]);
    end    
    
    % cost first derivatives
    xu_cost = @(xu) pendulum_cost(xu(ix,:),xu(iu,:));
    J       = squeeze(finite_difference(xu_cost, [x; u]));
    cx      = J(ix,:);
    cu      = J(iu,:);
    
    % cost second derivatives
    xu_Jcst = @(xu) squeeze(finite_difference(xu_cost, xu));
    JJ      = finite_difference(xu_Jcst, [x; u]);
    JJ      = 0.5*(JJ + permute(JJ,[2 1 3])); %symmetrize
    cxx     = JJ(ix,ix,:);
    cxu     = JJ(ix,iu,:);
    cuu     = JJ(iu,iu,:);
    
    [f,c] = deal([]);
end


function J = finite_difference(fun, x, h)
% simple finite-difference derivatives
% assumes the function fun() is vectorized

if nargin < 3
    h = 2^-17;
end

[n, K]  = size(x);
H       = [zeros(n,1) h*eye(n)];
H       = permute(H, [1 3 2]);
X       = pp(x, H);
X       = reshape(X, n, K*(n+1));
Y       = fun(X);
m       = numel(Y)/(K*(n+1));
Y       = reshape(Y, m, K, n+1);
J       = pp(Y(:,:,2:end), -Y(:,:,1)) / h;
J       = permute(J, [1 3 2]);

% utility functions: singleton-expanded addition and multiplication
function c = pp(a,b)
c = bsxfun(@plus,a,b);

function c = tt(a,b)
c = bsxfun(@times,a,b);