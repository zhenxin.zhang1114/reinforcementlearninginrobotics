function Q_table = UpdateSARSA( s, a, r, sp, ap, tab , alpha, gamma )
% UpdateQ update de Qtable and return it using Whatkins QLearing
% s1: previous state before taking action (a)
% s2: current state after action (a)
% r: reward received from the environment after taking action (a) in state
%                                             s1 and reaching the state s2
% a:  the last executed action
% tab: the current Qtable
% alpha: learning rate
% gamma: discount factor
% Q: the resulting Qtable

Q_table = tab;
Q_table(s,a) =  Q_table(s,a) + alpha * ( r + gamma*tab(sp,ap) - tab(s,a) );