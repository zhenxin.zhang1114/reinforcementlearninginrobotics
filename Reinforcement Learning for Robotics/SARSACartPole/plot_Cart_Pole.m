function plot_Cart_Pole(s,action,steps)

x     = s(1);
theta = s(3);
l=3;
%l=3/(1+abs(theta));     %pole's Length for ploting it can be different from the actual length

pxg = [x+1 x-1 x-1 x+1 x+1];
pyg = [0.25 0.25 1.25 1.25 0.25];

pxp=[x x+l*sin(theta)];
pyp=[1.25 1.25+l*cos(theta)];

arrowfactor_x=sign(action)*2.5;
if (sign(arrowfactor_x)>0)
    text_arrow = strcat('==>> ',int2str(10*action));
else if (sign(arrowfactor_x)<0)
        text_arrow = strcat(int2str(10*action),' <<==');
    else
        text_arrow='=0=';
        arrowfactor_x=0.25;
    end
end

    
    
subplot(2,1,1);

%Car 
fill(pxg,pyg,[.6 .6 .5],'LineWidth',2);  %car
hold on
title(['Steps: ',int2str(steps)])

%Car Wheels
plot(x-0.5,0.25,'rO','LineWidth',2,'Markersize',20,'MarkerEdgeColor','k','MarkerFaceColor',[0.5 0.5 0.5]);
plot(x+0.5,0.25,'rO','LineWidth',2,'Markersize',20,'MarkerEdgeColor','k','MarkerFaceColor',[0.5 0.5 0.5]);

%Pendulum
%plot(pxp,pyp,'-rO','LineWidth',5,'Markersize',10,'MarkerEdgeColor','r','MarkerFaceColor','r');
plot(pxp,pyp,'-r','LineWidth',5);
plot(pxp(1),pyp(1),'.r','LineWidth',2,'Markersize',10,'MarkerEdgeColor','k','MarkerFaceColor','r');
plot(pxp(2),pyp(2),'rO','LineWidth',2,'Markersize',15,'MarkerEdgeColor','k','MarkerFaceColor','r');

%text(x + arrowfactor_x - 0.5 ,0.8,text_arrow);
%axis([x-6 x+6 0 6])
axis([-6 6 0 6])
%grid
box off
drawnow;
hold off


% *Summary:* Draw the cart-pole system with reward, applied force, and 
% predictive uncertainty of the tip of the pendulum
%
%    function draw_cart_pole(x, theta, force)
%
%
% *Input arguments:*
%
%		x          position of the cart
%   theta      angle of pendulum
%   force      force applied to cart
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Modified by Matteo Saveriano: 2018-10-17

% function draw_cart_pole(x, theta, force)
% % Parameters
% l = 0.6;
% xmin = -3; 
% xmax = 3;    
% height = 0.1;
% width  = 0.3;
% maxU = 10;
% 
% % Compute positions 
% cart = [ x + width,  height
%          x + width, -height
%          x - width, -height
%          x - width,  height
%          x + width,  height ];
% pendulum = [x, 0; x+2*l*sin(theta-pi), -cos(theta)*2*l];
% 
% 
% clf; hold on
% plot(0,2*l,'k+','MarkerSize',20,'linewidth',2)
% plot([xmin, xmax], [-height-0.03, -height-0.03],'k','linewidth',2)
% 
% % Plot force
% plot([0 force/maxU*xmax],[-0.3, -0.3],'g','linewidth',10)
% 
% % Plot the cart-pole
% fill(cart(:,1), cart(:,2),'k','edgecolor','k');
% plot(pendulum(:,1), pendulum(:,2),'r','linewidth',4)
% 
% % Plot the joint and the tip
% plot(x,0,'y.','markersize',24)
% plot(pendulum(2,1),pendulum(2,2),'y.','markersize',24)
% 
% % Text
% text(0,-0.3,'applied force')
% set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[-1.4 1.4]);
% axis off;
% drawnow;
% 


% function plot_Cart_Pole(s,action,steps)
% 
% x     = s(1);
% theta = s(3);
% l=3;
% %l=3/(1+abs(theta));     %pole's Length for ploting it can be different from the actual length
% 
% pxg = [x+1 x-1 x-1 x+1 x+1];
% pyg = [0.25 0.25 1.25 1.25 0.25];
% 
% pxp=[x x+l*sin(theta)];
% pyp=[1.25 1.25+l*cos(theta)];
% 
% arrowfactor_x=sign(action)*2.5;
% if (sign(arrowfactor_x)>0)
%     text_arrow = strcat('==>> ',int2str(10*action));
% else if (sign(arrowfactor_x)<0)
%         text_arrow = strcat(int2str(10*action),' <<==');
%     else
%         text_arrow='=0=';
%         arrowfactor_x=0.25;
%     end
% end
% 
%     
%     
% subplot(2,1,1);
% 
% %Car 
% fill(pxg,pyg,[.6 .6 .5],'LineWidth',2);  %car
% hold on
% title(['Steps: ',int2str(steps)])
% 
% %Car Wheels
% plot(x-0.5,0.25,'rO','LineWidth',2,'Markersize',20,'MarkerEdgeColor','k','MarkerFaceColor',[0.5 0.5 0.5]);
% plot(x+0.5,0.25,'rO','LineWidth',2,'Markersize',20,'MarkerEdgeColor','k','MarkerFaceColor',[0.5 0.5 0.5]);
% 
% %Pendulum
% %plot(pxp,pyp,'-rO','LineWidth',5,'Markersize',10,'MarkerEdgeColor','r','MarkerFaceColor','r');
% plot(pxp,pyp,'-r','LineWidth',5);
% plot(pxp(1),pyp(1),'.r','LineWidth',2,'Markersize',10,'MarkerEdgeColor','k','MarkerFaceColor','r');
% plot(pxp(2),pyp(2),'rO','LineWidth',2,'Markersize',15,'MarkerEdgeColor','k','MarkerFaceColor','r');
% 
% %text(x + arrowfactor_x - 0.5 ,0.8,text_arrow);
% %axis([x-6 x+6 0 6])
% axis([-6 6 0 6])
% %grid
% box off
% drawnow;
% hold off


% *Summary:* Draw the cart-pole system with reward, applied force, and 
% predictive uncertainty of the tip of the pendulum
%
%    function draw_cart_pole(x, theta, force)
%
%
% *Input arguments:*
%
%		x          position of the cart
%   theta      angle of pendulum
%   force      force applied to cart
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Modified by Matteo Saveriano: 2018-10-17

% function draw_cart_pole(x, theta, force)
% % Parameters
% l = 0.6;
% xmin = -3; 
% xmax = 3;    
% height = 0.1;
% width  = 0.3;
% maxU = 10;
% 
% % Compute positions 
% cart = [ x + width,  height
%          x + width, -height
%          x - width, -height
%          x - width,  height
%          x + width,  height ];
% pendulum = [x, 0; x+2*l*sin(theta-pi), -cos(theta)*2*l];
% 
% 
% clf; hold on
% plot(0,2*l,'k+','MarkerSize',20,'linewidth',2)
% plot([xmin, xmax], [-height-0.03, -height-0.03],'k','linewidth',2)
% 
% % Plot force
% plot([0 force/maxU*xmax],[-0.3, -0.3],'g','linewidth',10)
% 
% % Plot the cart-pole
% fill(cart(:,1), cart(:,2),'k','edgecolor','k');
% plot(pendulum(:,1), pendulum(:,2),'r','linewidth',4)
% 
% % Plot the joint and the tip
% plot(x,0,'y.','markersize',24)
% plot(pendulum(2,1),pendulum(2,2),'y.','markersize',24)
% 
% % Text
% text(0,-0.3,'applied force')
% set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[-1.4 1.4]);
% axis off;
% drawnow;
% 
