function  Cart_PoleDemo( maxepisodes )

global grafica
maxsteps    = 1000;              % maximum number of steps per episode
statelist   = BuildStateList();  % the list of states
actionlist  = BuildActionList(); % the list of actions

nstates     = size(statelist,1);
nactions    = size(actionlist,1);
Q           = BuildQTable( nstates,nactions );  % the Qtable

alpha       = 0.3;   % learning rate
gamma       = 1.0;   % discount factor
epsilon     = 0.001;  % probability of a random action selection
grafica     = false; % indicates if display the graphical interface

xpoints     = [];
ypoints     = [];
global state_every_iter
for i=1:maxepisodes    
    
    
    [total_reward,steps,Q ] = Episode( maxsteps, Q , alpha, gamma,epsilon,statelist,actionlist,grafica ); 
    
    %disp(['Espisode: ',int2str(i),'  Steps:',int2str(steps),'  Reward:',num2str(total_reward),' epsilon: ',num2str(epsilon)])
    Q_Table=Q;
    All_episodes_state{i}=state_every_iter;
    epsilon = epsilon * 0.99;
    
    xpoints(i)=i-1;
    ypoints(i)=steps;    
    subplot(2,1,2);    
    plot(xpoints,ypoints)      
    title(['Episode: ',int2str(i),' epsilon: ',num2str(epsilon)])  
    xlabel('Episodes')
    ylabel('Steps')    
    drawnow
    
    if (i>1000)
        grafica=true;
    end
end






