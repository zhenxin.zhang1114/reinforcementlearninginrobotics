function QlearnPend
%  Based on the Example reinforcement learning - Q-learning code of Matthew
%  Sheen, 2015.
%  A lot modifications such as rewardFunc, torque, learnRate, epsilon,
%  angle ranges, RK4 loop, dynamics are done by Xuyi Wu, 2019, in order to
%  make the training as quick as possible and the results suitable for the
%  tasks of the Reiinforcement Learning for Robotics lecture.

close all;

%% SETTINGS

%rewardfunction
rewardFunc = @(x,xdot)(-10*(abs(x-pi)).^2 + -10*0.25*(abs(xdot)).^2);

learnRate = 0.9;

% epislon-greedy is used here, because the initial Q values in the state
% are same for the different actions
epsilon = 0.3; % Initial value
epsilonDecay = 0.8; % Decay factor per iteration.

%%% Future vs present value
discount = 0.9; 

%%% Inject some noise?
successRate = 1; % How often do we do what we intend to do?
% E.g. What if I'm trying to turn left, but noise causes
% me to turn right instead. This probability (0-1) lets us
% try to learn policies robust to "accidentally" doing the
% wrong action sometimes.

% Option to give a very large bonus when the system reaches the desired state (pendulum upright).
winBonus = 300;

% Start every episode at vertical down. Angles range is from 0 to 2pi.
startPt = [0,0]; 

% Each episode is starting with the pendulum down and doing continuous actions for awhile.
maxEpi = 1000;
maxit = 1500;
% for RK-loop
substeps = 2;
dt = 0.05;

% Torque limits 
% tLim = 10;
% tLim2 = 5;
% tLim3 = 1;
% actions = [0, -tLim, -tLim2, -tLim3, tLim3, tLim2, tLim];

actions = [0, -6, -4, -2, 2, 4, 6];

% Make the un-updated values on the value map transparent. If not, then
% we see the reward function underneath.
transpMap = true;

% Write to video?
doVid = false;

if doVid
    writerObj = VideoWriter('qlearnVid.mp4','MPEG-4');
    writerObj.FrameRate = 60;
    open(writerObj);
end

%% Discretize the state so we can start to learn a value map

% State1 angle
x1 = 0:0.1:2*pi;
% State2 angular rate
% Large range because of high torque limits
x2 = -2*pi:0.2:2*pi;

%Generate a state list
states=zeros(length(x1)*length(x2),2); % 2 Column matrix of all possible combinations of the discretized state.
index=1;
for j=1:length(x1)
    for k = 1:length(x2)
        states(index,1)=x1(j);
        states(index,2)=x2(k);
        index=index+1;
    end
end

% Reward table
R = rewardFunc(states(:,1),states(:,2));
%initialize the Q table according R
Q = repmat(R,[1,length(actions)]); 

%for plots
Vreward = [];
Vstate1 = [];
Vstate2 = [];
Vstatelast1 = [];
Vstatelast2 = [];
Vtorquelast = [];

% V will be the best of the Q actions at every state. This is only
% important for my plotting. Not algorithmically significant. I leave
% values we haven't updated at 0, so they appear as blue (unexplored) areas
% in the plot.
%V = zeros(size(states,1),1);
%Vorig = reshape(max(Q,[],2),[length(x2),length(x1)]);

%% Set up the pendulum plot
panel = figure;
panel.Position = [680 558 1000 400];
panel.Color = [1 1 1];
subplot(1,4,1)

hold on
% Axis for the pendulum animation
f = plot(0,0,'b','LineWidth',10); % Pendulum stick
axPend = f.Parent;
axPend.XTick = []; % No axis stuff to see
axPend.YTick = [];
axPend.Visible = 'off';
axPend.Position = [0.01 0.5 0.3 0.3];
axPend.Clipping = 'off';
axis equal
axis([-1.2679 1.2679 -1 1]);
plot(0.001,0,'.k','MarkerSize',50); % Pendulum axis point

hold off

%% Set up the state-value map plot (displays the value of the best action at every point)
%colormap('hot');
%subplot(1,4,[2:4]);
%hold on
%map = imagesc(reshape(R,[length(x2),length(x1)]));
%axMap = map.Parent;
%axMap.XTickLabels = {'0' 'pi' '2*pi'};
%axMap.XTick = [1 floor(length(x1)/2) length(x1)];
%axMap.YTickLabels = {'-pi' '0' 'pi'};
%axMap.YTick = [1 floor(length(x2)/2) length(x2)];
%axMap.XLabel.String = 'Angle (rad)';
%axMap.YLabel.String = 'Angular rate (rad/s)';
%axMap.Visible = 'on';
% axMap.Color = [0.3 0.3 0.5];
% axMap.XLim = [1 length(x1)];
% axMap.YLim = [1 length(x2)];
% axMap.Box = 'off';
% axMap.FontSize = 14;
% caxis([3*min(R),max(R)])
% pathmap = plot(NaN,NaN,'.g','MarkerSize',30); % The green marker that travels through the state map to match the pendulum animation
% map.CData = V;
% hold off

%% Start learning!
steplast = 0;
tic;

% Number of episodes or "resets"
for episodes = 1:maxEpi
    
    z1 = startPt; % Reset the pendulum on new episode.
    reward = 0;
    % Number of actions we're willing to try before a reset
    for g = 1:maxit
        
        % Stop if the figure window is closed.
        if ~ishandle(panel)
            break;
        end
        
        %% PICK AN ACTION
        
        % Interpolate the state within our discretization (ONLY for choosing
        % the action. We do not actually change the state by doing this!)
        [~,sIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2));
        
        %record reward
        %R(sIdx)
        reward = reward + R(sIdx);
            
        if (episodes == maxEpi)
            Vstatelast1 = [Vstatelast1 states(sIdx,1)];
            Vstatelast2 = [Vstatelast2 states(sIdx,2)];
            steplast = steplast +1;
        end
        % Choose an action:
        % EITHER 1) pick the best action according the Q matrix (EXPLOITATION). OR
        % 2) Pick a random action (EXPLORATION)
        if (rand()>epsilon || episodes == maxEpi) && rand()<=successRate % Pick according to the Q-matrix it's the last episode or we succeed with the rand()>epsilon check. Fail the check if our action doesn't succeed (i.e. simulating noise)
            [~,aIdx] = max(Q(sIdx,:)); % Pick the action the Q matrix thinks is best!
        else
            aIdx = randi(length(actions),1); % Random action!
        end
        
        T = actions(aIdx);
        
        if (episodes == maxEpi)
            Vtorquelast = [Vtorquelast T];
        end
        
        %% STEP DYNAMICS FORWARD
        % RK4 Loop - Numerical integration
        for i = 1:substeps
            k1 = Dynamics(z1,T);
            k2 = Dynamics(z1+dt/2*k1,T);
            k3 = Dynamics(z1+dt/2*k2,T);
            k4 = Dynamics(z1+dt*k3,T);
            
            z2 = z1 + dt/6*(k1 + 2*k2 + 2*k3 + k4);

            %all states wrapped to 0-2pi
            if z2(1) >= 2*pi
                z2(1) = z2(1) - 2*pi;
            elseif z2(1) < 0
                z2(1) = 2*pi + z2(1);
            end
        end
        
        z1 = z2; % Old state = new state
        
        
        %% UPDATE Q-MATRIX
        
        % End condition for an episode
        if ( abs(z2(2))<0.01 & abs (z2(1) - pi)<0.01 )% If we've reached upright with no velocity (within some margin), end this episode.
            success = true;
            bonus = winBonus; % Give a bonus for getting there.
        else
            bonus = 0;
            success = false;
        end

        [~,snewIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2)); % Interpolate again to find the new state the system is closest to.
        
        if episodes ~= maxEpi % On the last iteration, stop learning and just execute. Otherwise...
            % Update Q
            Q(sIdx,aIdx) = Q(sIdx,aIdx) + learnRate * ( R(snewIdx) + discount*max(Q(snewIdx,:)) - Q(sIdx,aIdx) + bonus );   
        end
        
        % Decay the odds of picking a random action vs picking the
        % estimated "best" action. I.e. we're becoming more confident in
        % our learned Q.
        epsilon = epsilon*epsilonDecay;
        
        %% UPDATE PLOTS
        
        if episodes>0
            % Pendulum state:
            set(f,'XData',[0 sin(z1(1))]);
            set(f,'YData',[0 -cos(z1(1))]);
            
            % Green tracer point:
%             [newy,newx] = ind2sub([length(x2),length(x1)],snewIdx); % Find the 2d index of the 1d state index we found above
%             set(pathmap,'XData',newx);
%             set(pathmap,'YData',newy);
%             
%             % The heat map of best Q values
%             V = max(Q,[],2); % Best estimated value for all actions at each state.
%             fullV = reshape(V,[length(x2),length(x1)]); % Make into 2D for plotting instead of a vector.
%             set(map,'CData',fullV);
%             if transpMap
%                 set(map,'AlphaData',fullV~=Vorig); % Some spots have not changed from original. If not, leave them transparent.
%             end
            drawnow;
            
            % Take a video frame if turned on.            if doVid
            %    frame = getframe(panel);
            %    writeVideo(writerObj,frame);
        end
        
        % End this episode if we've hit the goal point (upright pendulum).
        if success
            break;
        end
       
    end
    
    reward
    Vreward = [Vreward reward];
    Vstate1 = [Vstate1 states(sIdx,1)];
    Vstate2 = [Vstate2 states(sIdx,2)];
end

% plot for reward in each episode
Time=1:1:maxEpi;
figure
plot(Time,Vreward)
title('Reward Plot in Each Episode')
xlabel('episode')
ylabel('reward')

% plot for last states in each episode
figure
plot(Time,Vstate1,Time, Vstate2);
title('Last States in Each Episode')
xlabel('episode')
ylabel('states')
legend({'state1 angle','state2 angular speed'}, 'location','northeast')

%plot for the states and torque in the last episode
Timelast = 1:1:steplast;
figure
plot (Timelast, Vstatelast1,Timelast, Vstatelast2, Timelast, Vtorquelast);
title('States and Torque in the Last Episode')
xlabel('episode')
legend({'state1 angle','state2 angular speed','torque'}, 'location','northeast')

%if doVid
%    close(writerObj);
%end

toc;
t=toc
end


function zdot =  Dynamics(z,u)
%% Code

l = 1;    % [m]        length of pendulum
m = 1;    % [kg]       mass of pendulum
g = 9.82; % [m/s^2]    acceleration of gravity
c = 0.2; % [s*Nm/rad] friction coefficient

zdot = zeros(1,2);
zdot(2) = ( u - c*z(2) - m*g*l*sin(z(1))/2 ) / (m*l^2/3);
zdot(1) = z(2);
end
