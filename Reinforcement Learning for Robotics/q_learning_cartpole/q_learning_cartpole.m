
close all;

%% SETTINGS
rewardFunc = @(x1,x2,x3,x4)(-10*(abs(x1)).^2 +(-5*(abs(x2)).^2)+ (-20*(abs(x3-pi)).^2) + (-5*(abs(x4)).^2)); 
learnRate = 0.9; 
epsilon = 0.5; 
epsilonDecay = 0.98; 
discount = 0.9; 
successRate = 1; 
winBonus = 1000; 
startPt = [0,0,pi-0.2,0]; 
maxEpi = 2000;
maxit = 1500; 
dt = 0.01; 
%% build action list
actions = -1.0:0.1:1.0;

%% build state list
x1div = (2-(-2)) / 3.0;
x2div = (0.1-(-0.1)) / 2.0;
x3div = (deg2rad(12)-(deg2rad(-12)))/8.0;
x4div = (deg2rad(10)-(deg2rad(-10)))/2.0;

%x1  = -2:x1div:2;
x1  = [-1  1];
%x2  = -0.5:x2div:0.5;
x2  = [-1 0 1];
x3  = deg2rad(-12):x3div:deg2rad(12);
%x4  = deg2rad(-10):x4div:deg2rad(10);
x4   = [-1  1];


I=size(x1,2);
J=size(x2,2);
K=size(x3,2);
L=size(x4,2);
states=[];
index=1;
for i=1:I    
    for j=1:J
        for k=1:K
            for l=1:L
                states(index,1)=x1(i);
                states(index,2)=x2(j);
                states(index,3)=x3(k);
                states(index,4)=x4(l);
                index=index+1;
            end
        end
    end
end

%% Discretize the state so we can start to learn a value map
x1=-2:0.2:2; %location
x2=-4:0.2:4; %velocity of car
x3 = 0:0.1*pi:2*pi;   %angle
x4 = -20:0.5:20; %angle velocity
%% generate state list
states=zeros(length(x1)*length(x2)*length(x3)*length(x4),1); 
index=1;
for j=1:length(x1)
    for k = 1:length(x2)
        for l = 1:length(x3)
            for m = 1:length(x4)
                states(index,1)=x1(j);
                states(index,2)=x2(k);
                states(index,3)=x3(l);
                states(index,4)=x4(m);
                index=index+1;
            end
        end
    end
end
R = rewardFunc(states(:,1),states(:,2),states(:,3),states(:,4)); 
Q = repmat(R,[1,size(actions,2)]);
%% Start learning!
total_data={};
for episodes = 1:maxEpi
    z1 = startPt; 
    iter_data=[];
    force=[];
    for g = 1:maxit 
        disp([episodes,g])
        [~,sIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2));
        if (rand()>epsilon || episodes == maxEpi) && rand()<=successRate 
            [~,aIdx] = max(Q(sIdx,:)); 
        else
            aIdx = randi(length(actions),1); 
        end       
        T = actions(aIdx);
        force(g,:)=T;
        
        %% STEP DYNAMICS FORWARD
        iter_data(g,:)=z1;
        dz2=dynamics_cart_pole(z1,T);
        z2=z1+dz2*dt;        
        if z2(3)>2*pi
            while z2(3)>2*pi
                z2(3) = z2(3)-2*pi;
            end
            
        elseif z2(3)<0
            while z2(3)<0
                z2(3) = 2*pi+z2(3);
            end
        end
        disp(z2(3))
        z1 = z2; % Old state = new state
        
        
        %% UPDATE Q-MATRIX
        
        if norm(z2-[0,0,pi,0])<0.05 
            success = true;
            bonus = winBonus; 
        else
            bonus = 0;
            success = false;
        end
        
        [~,snewIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2)); 
        if episodes ~= maxEpi 
            Q(sIdx,aIdx) = Q(sIdx,aIdx) + learnRate * ( R(snewIdx) + discount*max(Q(snewIdx,:)) - Q(sIdx,aIdx) + bonus );
        end
        epsilon = epsilon*epsilonDecay;
        draw_cart_pole(z1(1),z1(3),T)
        
        %% UPDATE PLOTS    
        if abs(z2(1))>3
            bonus=-1000;
            success=true;
        end
        if success
            break;
        end
        
    end
    total_data{episodes}=iter_data;
%     text=['episode',num2str(episodes)]
%     figure(episodes+1)
%     plot(iter_data)
%     legend("locaiton","velocity","theta","angle velocity");
%     title(text)
%     h1 = figure(episodes+1);
%     name=['episode',num2str(episodes),'.jpg']
%     saveas(h1,name);
end


%animate_cart_pole(iter_data(:,1),iter_data(:,3),force)
