%% animate_cart_pole.m
% *Summary:* Draw the cart-pole system with reward, applied force, and 
% predictive uncertainty of the tip of the pendulum
%
%    function animate_cart_pole(x, theta, force)
%
% *Input arguments:*
%
%		x      Tx1 vector: position trajectory of the cart
%   theta      Tx1 vector: angle trajectory of pendulum
%   force      Tx1 vector: forces applied to cart
%
% Author: Matteo Saveriano

function animate_cart_pole(x, theta, force)
    for t = 1:size(x,1)
        draw_cart_pole(x(t,1), theta(t,1), force(t,1))
        pause(0.1);
    end

   
