
close all;

%% SETTINGS

%回报方程 @是函数句柄符号，表示关于某个变量的函数
rewardFunc = @(x,xdot)(-(abs(x)).^2 + -0.25*(abs(xdot)).^2); % Reward is -(quadratic error) from upright position. Play around with different things!

%%% 学习速率
learnRate = 0.99; % How is new value estimate weighted against the old (0-1). 1 means all new and is ok for no noise situations.

%%% Exploration vs. exploitation   探索和利用
% Probability of picking random action vs estimated best action
epsilon = 0.5; % Initial value
epsilonDecay = 0.98; % Decay factor per iteration.

%%% Future vs present value  gama 
discount = 0.9; % When assessing the value of a state & action, how important is the value of the future states?

%%% Inject some noise?
successRate = 1; % How often do we do what we intend to do?
% E.g. What if I'm trying to turn left, but noise causes
% me to turn right instead. This probability (0-1) lets us
% try to learn policies robust to "accidentally" doing the
% wrong action sometimes.

winBonus = 100;  % Option to give a very large bonus when the system reaches the desired state (pendulum upright).

startPt = [0;0]; % Start every episode at vertical down.  初始位置

maxEpi = 2000; % Each episode is starting with the pendulum down and doing continuous actions for awhile.
maxit = 10; % Iterations are the number of actions taken in an episode.
substeps = 2; % Number of physics steps per iteration (could be 1, but more is a little better integration of the dynamics)
dt = 0.05; % Timestep of integration. Each substep lasts this long

% Torque limits -- bang-bang control
tLim = 10;
actions = [0, -0.5*tLim,-tLim,0.5*tLim,tLim]; % Only 3 options, Full blast one way, the other way, and off.



%% Discretize the state so we can start to learn a value map
% State1 is angle -- play with these for better results. Faster convergence
% with rough discretization, less jittery with fine.
x1 = 0:0.05:2*pi;   % 角度
%State2 angular rate
x2 = -pi:0.1:pi;  % 角速度

%Generate a state list  生成状态表


states=zeros(2,length(x1)*length(x2)); % 2 Column matrix of all possible combinations of the discretized state.
index=1;
for j=1:length(x1)
    for k = 1:length(x2)
        states(1,index)=x1(j);
        states(2,index)=x2(k);
        index=index+1;
    end
end

states=states'



% Local value R and global value Q -- A very important distinction!
%
% R, the reward, is like a cost function. It is good to be near our goal. It doesn't
% account for actions we can/can't take. We use quadratic difference from the top.
%
% Q is the value of a state + action. We have to learn this at a global
% level. Once Q converges, our policy is to always select the action with the
% best value at every state.
%
% Note that R and Q can be very, very different.
% For example, if the pendulum is near the top, but the motor isn't
% powerful enough to get it to the goal, then we have to "pump" the pendulum to
% get enough energy to swing up. In this case, that point near the top
% might have a very good reward value, but a very bad set of Q values since
% many, many more actions are required to get to the goal.

R = rewardFunc(states(:,1),states(:,2)); % Initialize the "cost" of a given state to be quadratic error from the goal state. Note the signs mean that -angle with +velocity is better than -angle with -velocity

% repmat 函数，将R复制，
Q = repmat(R,[1,size(actions,2)]); % Q is length(x1) x length(x2) x length(actions) - IE a bin for every action-state combination.

% V will be the best of the Q actions at every state. This is only
% important for my plotting. Not algorithmically significant. I leave
% values we haven't updated at 0, so they appear as blue (unexplored) areas
% in the plot.
V = zeros(size(states,1),1);
Vorig = reshape(max(Q,[],2),[length(x2),length(x1)]);


%% Start learning!

% Number of episodes or "resets"
for episodes = 1:maxEpi
    
    z1 = startPt; % Reset the pendulum on new episode.
    z1=z1';
    
    % Number of actions we're willing to try before a reset
    for g = 1:maxit      
        %% PICK AN ACTION
        disp(g);
        % Interpolate the state within our discretization (ONLY for choosing
        % the action. We do not actually change the state by doing this!)
        [~,sIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2));
        % Choose an action:
        % EITHER 1) pick the best action according the Q matrix (EXPLOITATION). OR
        % 2) Pick a random action (EXPLORATION)
        if (rand()>epsilon || episodes == maxEpi) && rand()<=successRate % Pick according to the Q-matrix it's the last episode or we succeed with the rand()>epsilon check. Fail the check if our action doesn't succeed (i.e. simulating noise)
            [~,aIdx] = max(Q(sIdx,:)); % Pick the action the Q matrix thinks is best!
        else
            aIdx = randi(length(actions),1); % Random action!
        end
        
        T = actions(aIdx);
        
        %% STEP DYNAMICS FORWARD
        
        % Step the dynamics forward with our new action choice
        % RK4 Loop - Numerical integration
        for i = 1:substeps
            z2=pendulum_dynamics(z1',T,dt);
            z2=z2';
            % All states wrapped to 2pi
            
        end
        
        z1 = z2; % Old state = new state
        
        
        %% UPDATE Q-MATRIX
        
        % End condition for an episode
        if ((z2(1)-pi)^2 + z2(2)^2)<0.01 % If we've reached upright with no velocity (within some margin), end this episode.
            success = true;
            bonus = winBonus; % Give a bonus for getting there.
        else
            bonus = 0;
            success = false;
        end
        
        [~,snewIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2)); % Interpolate again to find the new state the system is closest to.
        
        if episodes ~= maxEpi % On the last iteration, stop learning and just execute. Otherwise...
            % Update Q
            Q(sIdx,aIdx) = Q(sIdx,aIdx) + learnRate * ( R(snewIdx) + discount*max(Q(snewIdx,:)) - Q(sIdx,aIdx) + bonus );
        end
        epsilon = epsilon*epsilonDecay;
        
        %% UPDATE PLOTS
        
        if episodes>0
            
            draw_pendulum(z1(1),T,1);

        end
        
        % End this episode if we've hit the goal point (upright pendulum).
        if success
            break;
        end
        
    end
end



